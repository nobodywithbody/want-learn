package wantlearn.services;

import org.springframework.stereotype.Repository;
import wantlearn.form.RegisterUser;
import wantlearn.model.Learner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class UserService {

    @PersistenceContext
    public EntityManager entityManager;

    @Transactional
    public boolean register(RegisterUser userInfo) {
        Learner learner = new Learner(userInfo.getEmail());
        entityManager.persist(learner);
        return true;
    }
}
