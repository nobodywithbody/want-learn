package wantlearn.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import wantlearn.dto.SuccessDTO;
import wantlearn.form.RegisterUser;
import wantlearn.services.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    @ResponseBody
    public SuccessDTO register(@Valid @RequestBody RegisterUser userInfo) {
        userService.register(userInfo);
        return new SuccessDTO();
    }
}
