package wantlearn.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Learner {

    @Id
    @GeneratedValue
    @Getter @Setter  private long id;

    @Getter
    @Setter
    private String email;

    public Learner(String email) {
        this.email = email;
    }
}
