package wantlearn.form;

import lombok.Getter;
import lombok.Setter;
import wantlearn.constraits.FieldMatch;

import javax.validation.constraints.NotNull;

@FieldMatch(first="password", second="confirmPassword", message = "Пароли должны совпадать")
public class RegisterUser {
    @Getter
    @Setter
    @NotNull(message="Email должен быть указан")
    private String email;

    @Getter
    @Setter
    @NotNull(message="Пароль должен быть указан")
    private String password;

    @Getter
    @Setter
    @NotNull(message="Повторный пароль должен быть указан")
    private String confirmPassword;

}
