package wantlearn.validators;

import org.springframework.util.StringUtils;
import wantlearn.constraits.FieldMatch;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Method;

public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {
    private String firstFieldName;
    private String secondFieldName;

    @Override
    public void initialize(final FieldMatch constraintAnnotation) {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        try {
            Class c = value.getClass();
            String firstGetterName = StringUtils.capitalize(firstFieldName);
            String secondGetterName = StringUtils.capitalize(secondFieldName);
            System.out.println("get" + firstGetterName);
            System.out.println("get" + secondGetterName);
            Method firstMethod = c.getMethod("get" + firstGetterName);
            Method secondMethod = c.getMethod("get" + secondGetterName);
            String firstValue = (String) firstMethod.invoke(value);
            String secondValue = (String) secondMethod.invoke(value);

            return firstValue.equals(secondValue);
        } catch (final Exception ignore) {
        }
        return true;
    }
}
